<p align="center">
  <img src="https://i.imgur.com/FJhwrqv.jpg"/>
</p>


"Our goal is to deliver a seamless, immersive, and tailor-made experience, ensuring your essential customizations resonate with you, the community, and every individual."

# Build guide

Prior to building, you will need basic knowledge of [Git](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet).

### Requirements

- Around 600G disk space.
- A computer with at least 16GB RAM running Linux (recommended) or MacOS.
- Build environment [setup](https://github.com/akhilnarang/scripts).


# FireDroid Project #

### Sync ###

```bash

# Initialize local repository
 repo init -u https://gitlab.com/firedroid/manifest -b FIRE

# Sync
 repo sync -c -j$(nproc --all) --force-sync --no-clone-bundle --no-tags
```

### Build ###

```bash

# Set up environment
$ . build/envsetup.sh

# Choose a target
$ lunch aosp_$device-userdebug

# Build the code
$ mka bacon -j$(nproc --all)
```

### Compilation Help

To get help with build errors, please visit [**Android Building Help**](https://t.me/AndroidBuildingHelp).


# Maintainership

### Requirements
You don't need a lot of skills to be a device maintainer. You only need to have:
- enough git skills to properly handle your device specific repos, and
- the ability to read logs, so you can know any device related issues and fix them.

To apply for official status, please fill the maintainership application [**form**](https://forms.gle/Rk7Mepx7oBKSbvLz6).

### Links to official groups/channels
- [**Discussion Group**](https://t.me/firedroid_talks)
- [**Announcements Channel**](https://t.me/firedroid_official)
